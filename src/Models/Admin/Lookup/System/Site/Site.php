<?php

namespace Ecms\Translate\Models\Admin\Lookup\System\Site;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends Model {
	
	use SoftDeletes;
	
	protected static function boot() {
		parent::boot();
		
		static::addGlobalScope('module', function(Builder $builder) {
			$builder->where(function($query) {
				$query->whereNull('domain')->orWhere('domain', env('APP_DOMAIN'));
			});
		});
		
		// Order by id desc
		static::addGlobalScope('order', function(Builder $builder) {
			$builder->orderBy('record_priority', 'asc')->orderBy('id', 'desc');
		});
		
	}
}

<?php

namespace Ecms\Translate\Models\Triggers;

trait AutoDomain {
	protected static function bootAutoDomain() {
		static::creating(function($model) {
			$model->domain = env('APP_DOMAIN');
		});
	}
}


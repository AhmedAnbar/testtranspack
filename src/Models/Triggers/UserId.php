<?php

namespace Ecms\Translate\Models\Triggers;

use Illuminate\Support\Facades\Auth;

trait UserId {
	protected static function bootUserId() {
		static::creating(function($model) {
			//                $user = request()->user_token;
			$model->user_id = Auth::id() ? Auth::id() : null;
		});
	}
}

